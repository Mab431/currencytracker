﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CurrencyTracker
{
    [Activity(Label = "SortByActivity")]
    public class SortByActivity : Activity
    {
		Button btnSortName;
		Button btnSortVolume;
		Button btnSortPrice;
        Button btnSortSupply;
        Button btnSort24hr;
        Button btnSort12hr;
        Button btnSort3d;
        Button btnSort7d;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SortByLayout);
			// Create your application here

			//get refs
			btnSortName = FindViewById<Button>(Resource.Id.SortNameButton);
			btnSortPrice = FindViewById<Button>(Resource.Id.SortPriceButton);
            btnSortVolume = FindViewById<Button>(Resource.Id.SortTradeVolButton);
            btnSortSupply = FindViewById<Button>(Resource.Id.SortSupplyButton);
            btnSort24hr = FindViewById<Button>(Resource.Id.Sort24HrButton);
            btnSort12hr = FindViewById<Button>(Resource.Id.Sort12hrButton);
            btnSort3d = FindViewById<Button>(Resource.Id.Sort3dayButton);
            btnSort7d = FindViewById<Button>(Resource.Id.Sort7dayButton);


            //set delegates
            btnSortPrice.Click += delegate
			{
				CoinListSingleton.Instance.SortType = 1;
				Finish();
			};

            btnSortName.Click += delegate
            {
                CoinListSingleton.Instance.SortType = 9;
                Finish();
            };

            btnSortVolume.Click += delegate
            {
                CoinListSingleton.Instance.SortType = 2;
                Finish();
            };

            btnSortSupply.Click += delegate
            {
                CoinListSingleton.Instance.SortType = 5;
                Finish();
            };
            btnSort24hr.Click += delegate
            {
                CoinListSingleton.Instance.SortType = 7;
                Finish();
            };
            btnSort7d.Click += delegate
            {
                CoinListSingleton.Instance.SortType = 8;
                Finish();
            };
        }
    }
}

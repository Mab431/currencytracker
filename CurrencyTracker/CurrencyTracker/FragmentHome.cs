﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace CurrencyTracker
{
	public class Frag_Home : Fragment
	{
		View view;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			base.OnCreateView(inflater, container, savedInstanceState);
			view = inflater.Inflate(Resource.Layout.HomeLayout, container, false);

			//sort button
			Button btnSort = view.FindViewById<Button>(Resource.Id.btnSortCoins);
			btnSort.Click += delegate
			{
				view.Context.StartActivity(typeof(SortByActivity));
			};

			populateList();

			return view;
		}

		void populateList()
		{
			//Create coin list.
			ListView mainList = view.FindViewById<ListView>(Resource.Id.lst_Coins);
			List<string> coins = new List<string>();

			//Add coin names and tickers to coins list
			for (int i = 0; i < CoinListSingleton.Instance.SortedCoins.Count; i++)
			{
				coins.Add(CoinListSingleton.Instance.SortedCoins[i].name + " (" + CoinListSingleton.Instance.SortedCoins[i].symbol + ")");
			}

			//Display list
			updateView(mainList, coins);

			mainList.ItemClick += onItemClick;
		}

		void onItemClick(object source, AdapterView.ItemClickEventArgs args)
		{

			andyscoinclass.Item coinRef = CoinListSingleton.Instance.SortedCoins[args.Position];
			CoinListSingleton.SelectedCoin = coinRef;
			view.Context.StartActivity(typeof(AdvancedCoinView));

		}

		public override void OnResume()
		{
			base.OnResume();
			CoinListSingleton.Instance.SortedCoins = andyscoinclass.sortTheList(
				CoinListSingleton.Instance.RawData,
				CoinListSingleton.Instance.SortType);
			populateList();
		}

		void updateView(ListView src, List<string> list) {
			if (CoinListSingleton.Instance.SortedCoins.Count == 0)
			{
				string[] nodata = { "No Data" };
				src.Adapter = new ArrayAdapter<string>(view.Context, Android.Resource.Layout.SimpleListItem1, nodata);
			}
			else
			{
				src.Adapter = new ArrayAdapter<string>(view.Context, Android.Resource.Layout.SimpleListItem1, list);
			}
		}
	}
}
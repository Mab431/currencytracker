﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CurrencyTracker
{
	[Activity(Label = "Coin View")]
	public class AdvancedCoinView : Activity
	{
		EditText coinName; 
		EditText coinTicker;
		EditText coinTradeVolume;
		EditText coinPrice;
		EditText coinSupply;
		EditText coinChange24h;
		EditText coinChange12h;
		EditText coinChange3d;
		EditText coinChange7d;
		EditText coinChange1m;
		ToggleButton btnInvest;
		ToggleButton btnFavorite;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.CoinSelectedLayout);

			//check if coin exists
			if (CoinListSingleton.SelectedCoin == null) {
				Finish();
				return;
			}

			//set refs
			coinName = FindViewById<EditText>(Resource.Id.CoinCellNameText);
			coinTicker = FindViewById<EditText>(Resource.Id.CoinCellTickerText);
			coinTradeVolume = FindViewById<EditText>(Resource.Id.CoinCellTradeVolText);
			coinPrice = FindViewById<EditText>(Resource.Id.CoinCellPriceText);
			coinSupply = FindViewById<EditText>(Resource.Id.CoinCellSupplyText);
			coinChange24h = FindViewById<EditText>(Resource.Id.CoinCellTwoFourText);
			coinChange12h = FindViewById<EditText>(Resource.Id.CoinCellTwelveText);
			coinChange3d = FindViewById<EditText>(Resource.Id.CoinCellThreeText);
			coinChange7d = FindViewById<EditText>(Resource.Id.CoinCellSevenText);
			coinChange1m = FindViewById<EditText>(Resource.Id.CoinCellOneText);
			btnFavorite = FindViewById<ToggleButton>(Resource.Id.tgl_Favorite);
			btnInvest = FindViewById<ToggleButton>(Resource.Id.tgl_Invest);

			//set info
			andyscoinclass.Item c = CoinListSingleton.SelectedCoin;
			coinName.Text = c.name;
			coinTicker.Text = c.symbol;
			coinTradeVolume.Text = c.volume_usd_24h.ToString();
			coinPrice.Text = c.price_usd.ToString();
			coinSupply.Text = c.total_supply.ToString();
			coinChange24h.Text = c.percent_change_24h.ToString();
			coinChange12h.Text = c.percent_change_12h().ToString();
			coinChange3d.Text = c.percent_change_3day().ToString();
			coinChange7d.Text = c.percent_change_7d.ToString();
			coinChange1m.Text = c.percent_change_1month().ToString();

			itemIsFavorite(CoinListSingleton.SelectedCoin);
			btnFavorite.CheckedChange += OnToggleFavorite;
            btnInvest.CheckedChange += OnToggleInvest;
		}

        void OnToggleInvest(object source, CompoundButton.CheckedChangeEventArgs args)
        {
           
            Intent intent = new Intent(this, typeof(InvestmentSelectedActivity));
            StartActivity(intent);

        }

        void OnToggleFavorite(object source, CompoundButton.CheckedChangeEventArgs args) {
			bool b = args.IsChecked;
			if (b)
			{
				AddToFavorites(CoinListSingleton.SelectedCoin);
			}
			else {
				RemoveFromFavorites(CoinListSingleton.SelectedCoin);
			}
		}

		bool itemIsFavorite(andyscoinclass.Item coin) {
			bool exists = false;

			btnFavorite.Checked = false;
			foreach (var c in CoinListSingleton.Instance.FavoritesID) {
				if (c == coin.id) {
					exists = true;
					//set state
					btnFavorite.Checked = true;
					break;
				}
			}

			return exists;
		}

		void RemoveFromFavorites(andyscoinclass.Item coin) {
			CoinListSingleton.Instance.FavoritesID.Remove(coin.id);
			CoinListSingleton.Instance.saveFavoritesListToFile();
		}

		void AddToFavorites(andyscoinclass.Item coin) {
			CoinListSingleton.Instance.FavoritesID.Add(coin.id);
			CoinListSingleton.Instance.saveFavoritesListToFile();
		}

		public override void OnBackPressed()
		{
			CoinListSingleton.SelectedCoin = null;
			base.OnBackPressed();
		}
	}
}
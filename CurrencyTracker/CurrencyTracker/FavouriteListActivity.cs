﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CurrencyTracker
{
	//[Activity(Label = "FavoriteListActivity", MainLauncher = true, Icon = "@mipmap/icon")]
	[Activity(Label = "Favorites")]
	public class Frag_Favorites : Fragment
	{
		View view;

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);
			view = inflater.Inflate(Resource.Layout.FavouritesLayout, container, false);

			populateList();

			return view;
		}

		public override void OnResume()
		{
			base.OnResume();
			populateList();
		}

		void populateList()
		{
			//Create favorite list.
			ListView favoritesList = view.FindViewById<ListView>(Resource.Id.lst_Favorites);
			List<string> favorites = new List<string>();
			string[] noFavs = { "No Favorites" }; //Display if favorites list is empty

			//Add items to favorites list
			AddFavorites();

			//Add coin names and tickers to favorites list
			for (int i = 0; i < CoinListSingleton.Instance.Favorites.Count; i++)
			{
				favorites.Add(CoinListSingleton.Instance.Favorites[i].name + " (" + CoinListSingleton.Instance.Favorites[i].symbol + ")");
			}

			//Display list
			if (CoinListSingleton.Instance.Favorites.Count == 0)
			{
				favoritesList.Adapter = new ArrayAdapter<string>(view.Context, Android.Resource.Layout.SimpleListItem1, noFavs);
			}
			else
			{
				favoritesList.Adapter = new ArrayAdapter<string>(view.Context, Android.Resource.Layout.SimpleListItem1, favorites);
			}

			favoritesList.ItemClick += onItemClick;
		}

		void onItemClick(object source, AdapterView.ItemClickEventArgs args)
		{
            try
            {
                andyscoinclass.Item coinRef = CoinListSingleton.Instance.Favorites[args.Position];
                CoinListSingleton.SelectedCoin = coinRef;
                view.Context.StartActivity(typeof(AdvancedCoinView));
            }
            catch (ArgumentOutOfRangeException e)
            {
                // When no favorties show a toast
                Toast.MakeText(Application.Context, ((TextView)args.View).Text, ToastLength.Short).Show();
            }
		}

		void AddFavorites() {
			CoinListSingleton.Instance.Favorites = new List<andyscoinclass.Item>();
			foreach (var c in CoinListSingleton.Instance.SortedCoins) {
				foreach (var s in CoinListSingleton.Instance.FavoritesID) {
					if (c.id == s) {
						CoinListSingleton.Instance.Favorites.Add(c);
						break;
					}
				}
			}
		}
	}
}

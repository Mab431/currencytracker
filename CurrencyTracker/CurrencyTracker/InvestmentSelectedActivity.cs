﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace CurrencyTracker
{
    [Activity(Label = "Investments")]
    public class InvestmentSelectedActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.InvestmentSelectedLayout);
            andyscoinclass.Item c = CoinListSingleton.SelectedCoin; //get the selected coin maybe null
            investment i = CoinListSingleton.SelectedInvestment; // get the selected investment my be null
            var cointxt = FindViewById<EditText>(Resource.Id.InvCoinText); //coin name text box
            var qtytxt = FindViewById<EditText>(Resource.Id.InvQuantityText); // quantity text box
            var costtxt = FindViewById<EditText>(Resource.Id.InvPriceText); // price text box
            Button button = FindViewById<Button>(Resource.Id.InvConfirmButton); // confirm button
            Button cancel = FindViewById<Button>(Resource.Id.InvBackButton); // back / delete button
            String id = " ";
            String name = " ";
            Random r = new Random(); // initialize random number generator to produce new ids
            double quantity = 100;
            double cost = 50.25;
            bool edit = false; // variable for if this is an edit or creating new

            andyscoinclass.Item coinRef = CoinListSingleton.Instance.RawData[0]; // get bitcoin data for dummy data
            cointxt.Text = coinRef.name.ToString();
            costtxt.Text = coinRef.price_usd.ToString();
            qtytxt.Text = "1";
			// fill text boxes with dummy data ^^^^^^^^^


			string filename = TabActivity.InvestmentsFilePath;
            List<investment> investments = null; //initialize list to add to or edit of investments

            if (File.Exists(filename))
            {
                using (var streamReader = new StreamReader(filename))
                {

                    string content = streamReader.ReadToEnd();
                    investments = JsonConvert.DeserializeObject<List<investment>>(content); // fill investments list

                }
            }

            if (investments == null)
            {

                investments = new List<investment>(); // if the fill was empty the list is now null so initialize

            }
            


            if (c != null) // is this coming from a coin info page c contains info about the coin if the user clicked invest on the coin info page
            {

                cointxt.Text = c.name;
                costtxt.Text = c.price_usd;

            }

            if (i != null)  // is this coming from the invest page // an edit // if the user click an investment on the investment page i contain info about the coin
            {
                edit = true; // we know this is an edit so set this to true
                cointxt.Text = i.name;
                costtxt.Text = Convert.ToString(i.cost);
                qtytxt.Text = Convert.ToString(i.amount);
                //^^^ fill textboxes with info about the coin
                CoinListSingleton.SelectedInvestment = null;
                cancel.Text = "Delete"; //since this is an edit set the cancel button to a delete button 
                

            }

            cancel.Click += delegate //cancel / delete call 
            {

                if(i != null) // if we are editing this button handles deletes
                {

                    var item = investments.FirstOrDefault(o => o.id == i.id); // find the item we are editing in the list by id
                    investments.Remove(item); // delete that item

                    using (var streamWriter = new StreamWriter(filename, false)) // write the list to json now without the item we deleted
                    {
                        string output = JsonConvert.SerializeObject(investments);
                        streamWriter.Write(output);
                    }

                }
                

                Finish();

            };
            
            


                try
                {

                button.Click += delegate // if the user hits confirm 
                {
                    bool found = false; // variable did verify the coin the user entered matches a coin we have info on
                    name = cointxt.Text;
                    foreach (andyscoinclass.Item item in CoinListSingleton.Instance.RawData) // for every coin we have info on 
                    {

                        if (item.name.ToLower() == name.ToLower()) // check if the coin name the user input matches a coin we have info on
                        {

                            found = true; // we matched a coin
                            try
                            {
                                // coin matches so fill in the data to create an investment later
                                id = item.id;
                                name = item.name;
                                quantity = Convert.ToDouble(qtytxt.Text);
                                cost = Convert.ToDouble(costtxt.Text);
                                break;
                            }
                            catch
                            {
                                // something was empty or somebody put something wrong in
                                found = false;
                                Toast.MakeText(Application.Context, " Form Error ", ToastLength.Short).Show();
                                break;


                            }

                        }
                        else
                        {
                            // coin name didnt match user put a bad coin name in
                            found = false;
                            continue;

                        }



                    }

                    if (!found)
                    {
                        // if a coin name doesnt match display to user
                        Toast.MakeText(Application.Context, " COIN NOT FOUND ", ToastLength.Short).Show();

                    }
                    else // if it does
                    {

                        if (edit) // check if this is an edit
                        {

                            edit = false;
                            var item = investments.FirstOrDefault(o => o.id == i.id);
                            item.amount = quantity;
                            item.cost = cost;
                            item.name = name;
                            item.id = i.id;
                            // if this was an edit change the info about the investment in the list

                        }
                        else
                        {
                            investments.Add(new investment(id, name, quantity, cost));
                            // if not this is a new investment so put a new investment in the list
                        }

                       // write the new list to the json file
                        using (var streamWriter = new StreamWriter(filename, false))
                        {
                            string output = JsonConvert.SerializeObject(investments);
                            streamWriter.Write(output);
                        }
                            Finish();
                        

                        //add coin to investment file
                        //return from whenst you came

                    }

                };

            }
            catch(Exception e)
            {

                // miscellanious errors just for good luck
                // should never occur

                Toast.MakeText(Application.Context, " Proccessing Error ", ToastLength.Short).Show();


            }


        }
    }
}

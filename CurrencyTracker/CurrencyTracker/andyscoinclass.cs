﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace CurrencyTracker
{
    public class andyscoinclass
    {

        public class Item
        {
            public string id { get; set; }
            public string name { get; set; } // 9
            public string symbol { get; set; }
            public string rank { get; set; }
            public string price_usd { get; set; } //sort code 1
            [JsonProperty(PropertyName = "24h_volume_usd")]   //since in c# variable names cannot begin with a number, you will need to use an alternate name to deserialize
            public string volume_usd_24h { get; set; } //sort code 2
            public string market_cap_usd { get; set; } //sort code 3
            public string available_supply { get; set; } //sort code 4
            public string total_supply { get; set; } //sort code 5
            public string percent_change_1h { get; set; } //sort code 6
            public string percent_change_24h { get; set; } //sort code 7
            public string percent_change_7d { get; set; } //sort code 8
            public string last_updated { get; set; } 

            public string percent_change_12h()
            {
                double temp;

                temp = double.Parse(this.percent_change_24h);
                temp = temp / 2.0;
                return temp.ToString();


            }

            public string percent_change_3day()
            {

                double temp;

                temp = double.Parse(this.percent_change_24h);
                temp = temp * 3.0;
                return temp.ToString();
            }

            public string percent_change_1month()
            {
                double temp;

                temp = double.Parse(this.percent_change_7d);
                temp = temp * 4.0;
                return temp.ToString();
            }
        }


        public static List<Item> get_Data(List<Item> permalist)
        {

            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format("https://api.coinmarketcap.com/v1/ticker/"));

            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            Console.WriteLine(WebResp.StatusCode);
            Console.WriteLine(WebResp.Server);

            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())   //modified from your code since the using statement disposes the stream automatically when done
            {
                StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }

            List<Item> items = JsonConvert.DeserializeObject<List<Item>>(jsonString);

            Console.WriteLine(items.Count);     //returns 921, the number of items on that page


            permalist = items;


            return permalist;

        }


        static public List<Item> sortTheList(List<Item> tobesorted, int sortcode)
        {

            List<Item> returnlist;

           

            if(sortcode == 1)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.price_usd)).ToList<Item>(); 
            }
            else if(sortcode == 2)
            {
                returnlist =  tobesorted.OrderByDescending(Item => float.Parse(Item.volume_usd_24h)).ToList<Item>();
            }
            else if (sortcode == 3)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.market_cap_usd)).ToList<Item>();
            }
            else if (sortcode == 4)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.available_supply)).ToList<Item>();
            }
            else if (sortcode == 5)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.total_supply)).ToList<Item>();
            }
            else if (sortcode == 6)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.percent_change_1h)).ToList<Item>();
            }
            else if (sortcode == 7)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.percent_change_24h)).ToList<Item>();
            }
            else if (sortcode == 8)
            {
                returnlist = tobesorted.OrderByDescending(Item => float.Parse(Item.percent_change_7d)).ToList<Item>();
            }
            else if (sortcode == 9)
            {
                returnlist = tobesorted.OrderByDescending(Item => Item.percent_change_7d).ToList<Item>();
            }


            else
            {
                returnlist = tobesorted;
            }

            return returnlist;

        }

    }

    
}
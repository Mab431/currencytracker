﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace CurrencyTracker
{

    public class investment // class for holding investments
    {


        public string id { get; set; } // investment id not to be confused with coin id
        public string name { get; set; }
        public double amount { get; set; }
        public double cost { get; set; }
        public double profit { get; set; }

        public investment(string id, string name, double amount, double cost)
        {
            this.id = id;
            this.name = name;
            this.amount = amount;
            this.cost = cost;
        }

        public investment()
        {


        }

        public override string ToString() // this is what gets output to the list view
        {
            return name + " ~ #Owned: " + amount + " ~ AvgCost: $" + cost + " ~ Earnings: $" + profit;
        }
    }


    [Activity(Label = "Edit Investment")]
    public class Frag_Investments : Fragment
    {
        View view;
		List<investment> investments = new List<investment>(); // list to hold stuff for the tableview

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.InvestmentsLayout, container, false);

            string filename = TabActivity.InvestmentsFilePath; // where the investment info is stored
            
            Button add = view.FindViewById<Button>(Resource.Id.addButton); // add button

            void onItemClick(object source, AdapterView.ItemClickEventArgs args) // click event for list view
            {

                investment coinRef = investments[args.Position]; 
                CoinListSingleton.SelectedInvestment = coinRef;
                Intent intent = new Intent(Context, typeof(InvestmentSelectedActivity));
               
                StartActivity(intent);

            }

            add.Click += delegate // add button event
            {
                
                Intent intent = new Intent(Context, typeof(InvestmentSelectedActivity));
                StartActivity(intent);

            };

			

            //display list and setup list item click event 
            ListView investmentslistview = view.FindViewById<ListView>(Resource.Id.InvestmentListView); 
            investmentslistview.Adapter = new ArrayAdapter<investment>(view.Context, Android.Resource.Layout.SimpleListItem1, investments);
            investmentslistview.ItemClick += onItemClick;

            return view;
        }

		void RefreshData() {
			string filename = TabActivity.InvestmentsFilePath;

			if (File.Exists(filename)) // dont open a non existent file
			{

				using (var streamReader = new StreamReader(filename))
				{
					string content = streamReader.ReadToEnd(); //get the json investment info
					investments = JsonConvert.DeserializeObject<List<investment>>(content); // add it to the list
				}
			}
			else
			{
				using (var sw = new StreamWriter(filename))
				{
					sw.Write("");
				}
			}

			if (investments == null) // if list is empty initialize and give it dummy data for show
			{
				investments = new List<investment>();
				//investments.Add(new investment("001", "YOURCOIN", 500, 10.65));
			}
			else // else calculate the profit for each item based off current price
			{
				foreach (investment i in investments)
				{
					foreach (andyscoinclass.Item item in CoinListSingleton.Instance.RawData)
					{
						if (i.name == item.name)
						{
							i.profit = (Convert.ToDouble(item.price_usd) - i.cost) * i.amount;
						}
						// profit = currentprice - purchase price * number of coins owned
					}
				}
			}

			ListView investmentslistview = view.FindViewById<ListView>(Resource.Id.InvestmentListView);
			investmentslistview.Adapter = new ArrayAdapter<investment>(view.Context, Android.Resource.Layout.SimpleListItem1, investments);
		}

		public override void OnResume()
		{
			base.OnResume();
			RefreshData();
		}
	}
}

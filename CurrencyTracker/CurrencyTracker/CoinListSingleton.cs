﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace CurrencyTracker
{
    public class CoinListSingleton
    {
		public static andyscoinclass.Item SelectedCoin = null;
        public static investment SelectedInvestment = null;
        private static CoinListSingleton m_instance;
		public static CoinListSingleton Instance
		{ 
			get 
			{
				if (m_instance == null)
				{
					m_instance = new CoinListSingleton();
				}
				return m_instance;
			} 
		}

		public List<andyscoinclass.Item> SortedCoins { get; set; }
		public List<andyscoinclass.Item> RawData { get; set; }
		public List<andyscoinclass.Item> Investments{ get; set; }
		public List<andyscoinclass.Item> Favorites{ get; set; }
		public int SortType = 0;

        public List<string> FavoritesID { get; set; }
        public DateTime currentTime;
        

        private CoinListSingleton()
        {
            //create main list
            SortedCoins = new List<andyscoinclass.Item>();
            loadMainListFromFile();

            //create investments list
			Investments = new List<andyscoinclass.Item>();

            //create favorites list
			Favorites = new List<andyscoinclass.Item>();
            FavoritesID = new List<string>();
            loadFavoritesListFromFile();
        }

        private void loadMainListFromFile()
        {
            currentTime = DateTime.Now;
			string TimeData;
			DateTime timeOfLastPull = new DateTime();
			//----------------------------------------------
			//Create time file if it does not exist
			if (File.Exists(TabActivity.LastPullFilePath))
			{
				using (var sr = new StreamReader(TabActivity.LastPullFilePath))
				{
					TimeData = sr.ReadToEnd();
				}

				try
				{
					timeOfLastPull = DateTime.ParseExact(TimeData, "h:mm:ss tt", null);
				}
				catch (Exception e) {
					// write new file
					using (var sw = new StreamWriter(TabActivity.LastPullFilePath))
					{
						sw.Write(currentTime.ToLongTimeString());
						timeOfLastPull = new DateTime();
					}
				}
			}
			else
			{
				using (var sw = new StreamWriter(TabActivity.LastPullFilePath))
				{
					sw.Write(currentTime.ToLongTimeString());
				}
			}
            //----------------------------------------------

			//If it's been less than one hour since last pull, load from file
			if (currentTime < timeOfLastPull.AddHours(1.0))
			{
				//If the file of all coins already exists load it into the MainList
				if (File.Exists(TabActivity.MainListFilePath))
				{
					// Read existing json data
					using (var sr = new StreamReader(TabActivity.MainListFilePath))
					{
						var jsonData = sr.ReadToEnd();
						// De-serialize all objects and add them to the list
						SortedCoins = JsonConvert.DeserializeObject<List<andyscoinclass.Item>>(jsonData);
					}
				}
				else //Else pull from the web, create a the file, and load into the MainList
				{
					SortedCoins = andyscoinclass.get_Data(null);
					saveMainListToFile();
					currentTime = DateTime.Now;
					using (var sw = new StreamWriter(TabActivity.LastPullFilePath))
					{
						sw.Write(currentTime.ToLongTimeString());
					}
				}
			}
			else //else pull from web
			{
				SortedCoins = andyscoinclass.get_Data(null);
				currentTime = DateTime.Now;
				using (var sw = new StreamWriter(TabActivity.LastPullFilePath))
				{
					sw.Write(currentTime.ToLongTimeString());
				}
				saveMainListToFile();
			}

			//set raw data
			RawData = SortedCoins;
        }

        public void saveMainListToFile()
        {
            var jasonData = JsonConvert.SerializeObject(SortedCoins);
			using (var sw = new StreamWriter(TabActivity.MainListFilePath)) {
				sw.Write(jasonData);
			}
        }

		private void loadFavoritesListFromFile()
		{
            if (File.Exists(TabActivity.FavoritesListFilePath))
            {
				string jsonData;
				// Read existing json data
				using (var sr = new StreamReader(TabActivity.FavoritesListFilePath)) {
					jsonData = sr.ReadToEnd();
				}

                // De-serialize all objects and add them to the list
                FavoritesID = JsonConvert.DeserializeObject<List<string>>(jsonData);

				//Add favorite coins to the favorite list based off the ID's
				//Do this as long as there is at least one favorite ID
				if (FavoritesID != null)
				{
					for (int i = 0; i < FavoritesID.Count; i++)
					{
						if (FavoritesID[i] == SortedCoins[i].id)
						{
							Favorites.Add(SortedCoins[i]);
						}
					}
				}
				else {
					FavoritesID = new List<string>();
				}
            }
            else
            {
				using (var sw = new StreamWriter(TabActivity.FavoritesListFilePath))
				{
					sw.Write("");
				}
            }
        }

		public void saveFavoritesListToFile()
		{
            var jsonData = JsonConvert.SerializeObject(FavoritesID);
			using (var sw = new StreamWriter(TabActivity.FavoritesListFilePath)) {
				sw.Write(jsonData);
			}
		}
    }
}
﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.Util;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CurrencyTracker
{
    [Activity(Label = "CurrencyTracker", MainLauncher = true, Icon = "@mipmap/icon")]
    public class TabActivity : Activity
    {
		//path for list files and DateTime file
		//public static string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
		public static string path;
		public static string FavoritesListFilePath;
		public static string MainListFilePath;
		public static string LastPullFilePath;
		public static string InvestmentsFilePath;

		//public static string FavoritesListFilePath = Path.Combine(path, "ListOfFavorites.txt");
		//public static string MainListFilePath = Path.Combine(path, "ListOfAllCoins.txt");
        //public static string LastPullFilePath = Path.Combine(path, "LastPullTime.txt");

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			path = CacheDir.AbsolutePath;
			FavoritesListFilePath = Path.Combine(path, "ListOfFavorites.txt");
			MainListFilePath = Path.Combine(path, "ListOfAllCoins.txt");
			LastPullFilePath = Path.Combine(path, "LastPullTime.txt");
			InvestmentsFilePath = Path.Combine(path, "Investments.txt");

			SetContentView(Resource.Layout.Main);
            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;

			//CoinListSingleton.Instance.MainCoins = andyscoinclass.get_Data(CoinListSingleton.Instance.MainCoins);

			AddTab("Home", -1, new Frag_Home());
            AddTab("Favorites", -1, new Frag_Favorites());
            AddTab("My Investments", -1, new Frag_Investments()); 
        }

        private void AddTab(string TabText, int iconResourceID, Fragment view){			

            var tab = ActionBar.NewTab();
            tab.SetText(TabText);

			// must set event handler before adding tab
			tab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
			{
				var fragment = this.FragmentManager.FindFragmentById(Resource.Id.tabFragmentView);
				if (fragment != null)
					e.FragmentTransaction.Remove(fragment);
				e.FragmentTransaction.Add(Resource.Id.tabFragmentView, view);
			};

			tab.TabUnselected += delegate (object sender, ActionBar.TabEventArgs e) {
				e.FragmentTransaction.Remove(view);
			};

			//add tab after all is made
			ActionBar.AddTab(tab);
		}

		private void OnTabSelected(object sender, ActionBar.TabEventArgs args)
		{
			var CurrentTab = (ActionBar.Tab)sender;

			args.FragmentTransaction.Add(Resource.Id.tabFragmentView, new Frag_Home());
		}

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);


        }
    }
}

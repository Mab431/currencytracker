﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
//----------------------------------------------------
//THIS FILE IS OBSOLETE! DO NOT USE
//Refer to andyscoinclass.cs and CoinListSingleton.cs
//----------------------------------------------------
namespace CurrencyTracker
{
    public class Coin
    {
        public String id {get; set;}
        public String name {get; set;}
        public String ticker {get; set;}
        public int tradeVolume {get; set;}
        public double price {get; set;}
        public double supply {get; set;}
        public double change_24h {get; set;}
        public double change_12h {get; set;}
        public double change_3d {get; set;}
        public double change_7d {get; set;}
        public double change_1m {get; set;}

        public Coin()
        {

        }

        public Coin(String id ="",String name ="",String ticker = "",int tradeVolume = 0,double price = 0,double supply = 0,
            double change_24h = 0,double change_12h = 0,double change_3d = 0,double change_7d = 0,double change_1m = 0)
        {
            this.id = id;
            this.name = name;
            this.ticker = ticker;
            this.tradeVolume = tradeVolume;
            this.price = price;
            this.supply = supply;
            this.change_24h = change_24h;
            this.change_12h = change_12h;
            this.change_3d = change_3d;
            this.change_7d = change_7d;
            this.change_1m = change_1m;
        }

        

        public override string ToString()
        {
            return base.ToString();
        }
    }
}